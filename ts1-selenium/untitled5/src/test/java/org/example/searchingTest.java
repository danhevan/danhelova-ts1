package org.example;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;

class searchingTest {
    static List<Article> testArticles() {
        WebDriver driver = new FirefoxDriver();
        signupLogin login = new signupLogin(driver);
        Random random =  new Random();
        // Nebudu používat vlastní email ani zakládat nový kvůli úkolu, protože mi to nepřijde
        // v druhém případě správné a v prvním bezpačné, proto dělám jen registraci, třída pro
        // přihlašování je též implementované ve třídě signupLogin, jen je nevyužité
        String email = "IHateThis" + random.nextInt() + "@seznam.cz";
        login.register(email, "Jmeno", "FamName", "jhdjhcidhž56");
        AdvancedSearch as = new AdvancedSearch(driver);
        as.acceptCookies();
        as.enterAllWords("Page Object Model");
        as.enterLeastWords("Sellenium Testing");
        as.selectIn();
        as.enterStartYear("2024");
        as.clickSubmit();
        return as.getSearchResults();
    }


    @ParameterizedTest
    @MethodSource("testArticles")
    void testSavedArticles(Article article) {
        String articleTitle = article.getTitle();
        String articleDoi = article.getDOI();
        String articleDate = article.getDate();
        WebDriver driver = new ChromeDriver();

        searching searching = new searching(driver);

        searching.rejectCookies();
        searching.enterSearchKeyword(articleTitle);
        searching.submitSearch();
        Article result = searching.result();
        assertEquals(articleDate, result.getDate());
        assertEquals(articleDoi, result.getDOI());
        driver.close();


    }
}