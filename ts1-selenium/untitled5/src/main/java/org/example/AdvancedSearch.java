package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;

public class AdvancedSearch {
    private WebDriver driver;

    public AdvancedSearch(WebDriver driver) {
        this.driver = driver;
        driver.get("https://link.springer.com/advanced-search");
        this.allWordsInput  = By.id("all-words");
        this.exactPhraseInput  = By.id("exact-phrase");
        this.leastWordsInput  = By.id("least-words");
        this.withoutWordsInput = By.id("without-words");
        this.titleIsInput = By.id("title-is");
        this.authorIsInput  = By.id("author-is");
        this.startYearInput  = By.id("facet-start-year");
        this.endYearInput = By.id("facet-end-year");
        this.submitButton  = By.id("submit-advanced-search");
    }
    private By allWordsInput;
    private By exactPhraseInput;
    private By leastWordsInput;
    private By withoutWordsInput ;
    private By titleIsInput;
    private By authorIsInput;
    private By startYearInput;
    private By endYearInput ;
    private By submitButton;

    public void acceptCookies(){
        driver.findElement(By.cssSelector("button[data-cc-action='accept']")).click();


    }


    public void enterAllWords(String words) {
        driver.findElement(allWordsInput).sendKeys(words);
    }

    public void enterExactPhrase(String phrase) {
        driver.findElement(exactPhraseInput).sendKeys(phrase);
    }

    public void enterLeastWords(String words) {
        driver.findElement(leastWordsInput).sendKeys(words);
    }

    public void enterWithoutWords(String words) {
        driver.findElement(withoutWordsInput).sendKeys(words);
    }

    public void enterTitleIs(String title) {
        driver.findElement(titleIsInput).sendKeys(title);
    }

    public void enterAuthorIs(String author) {
        driver.findElement(authorIsInput).sendKeys(author);
    }

    public void enterStartYear(String year) {
        driver.findElement(startYearInput).sendKeys(year);
    }

    public void enterEndYear(String year) {
        driver.findElement(endYearInput).sendKeys(year);
    }

    public void clickSubmit() {
        driver.findElement(submitButton).click();
    }
    public void selectIn(){
        WebElement selectElement = driver.findElement(By.id("date-facet-mode"));
        Select select = new Select(selectElement);
        select.selectByValue("in");
    }
    public List<Article> getSearchResults(){
    List<Article> articles = new ArrayList<>();
    List<WebElement> resultList = driver.findElements(By.cssSelector("li.app-card-open.app-card-open--has-image[data-test=search-result-item]"));
    int count = 1;
        for (WebElement resultElement : resultList) {
        // Title
        WebElement titleElement = resultElement.findElement(By.cssSelector("h3.app-card-open__heading a"));
        String title = titleElement.getText();
        // DOI
        String href = titleElement.getAttribute("href");
        String doi = href.substring(href.indexOf("/article/") + 9, href.indexOf("/", href.indexOf("/article/") + 9));
        // Publication date
        WebElement metaElement = resultElement.findElement(By.cssSelector("div.c-meta span[data-test=published]"));
        String publishedDate = metaElement.getText();

        WebElement metaElementContentType = resultElement.findElement(By.cssSelector("div.c-meta span[data-test=content-type]"));
        String contentType = metaElementContentType.getText();
        if(contentType.equals("Article")) {
            articles.add(new Article(title, doi, publishedDate, contentType));
            if (count == 4) {
                break;
            }

            count++;
        }
    }
        return articles;
}

}
