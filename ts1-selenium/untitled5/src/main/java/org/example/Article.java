package org.example;

public class Article {
    private String title;
    private String DOI;
    private String publicationDate;
    private String type;
    public Article(String title, String DOI, String publicationDate, String type) {
        this.title = title;
        this.DOI = DOI;
        this.publicationDate = publicationDate;
        this.type = type;
    }

    @Override
    public String toString() {
        return this.title+ "+"+this.DOI + "+"+ this.publicationDate + "\n";
    }

    public String getTitle(){
        return this.title;

    }
    public String getDOI(){
        return this.DOI;

    }
    public String getDate(){
        return this.publicationDate;

    }
}
