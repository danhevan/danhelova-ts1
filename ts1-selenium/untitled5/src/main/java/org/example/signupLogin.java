package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class signupLogin {
    private WebDriver driver;
    public signupLogin(WebDriver driver){
        this.driver = driver;

    }
    public void register(String email,String name, String familyName,  String password) {
        driver.get("https://link.springer.com/signup-login");
        driver.findElement(By.cssSelector("button[data-cc-action='reject']")).click();
        driver.findElement(By.id("login-email")).sendKeys(email);
        driver.findElement(By.id("email-submit")).click();
        driver.findElement(By.id("registration-given-names")).sendKeys(name);
        driver.findElement(By.id("registration-family-name")).sendKeys(familyName);
        driver.findElement(By.id("registration-password")).sendKeys(password);
        driver.findElement(By.id("registration-password-confirm")).sendKeys(password);
        ((JavascriptExecutor) driver)
                .executeScript("window.scrollTo(0, document.body.scrollHeight)");
        WebElement checkbox = driver.findElement(By.id("registration-terms-conditions"));
        Actions actions = new Actions(driver);
        actions.moveToElement(checkbox).click().perform();
        driver.findElement(By.id("form-registration-submit")).click();



    }

    public void login(String email, String password){
        driver.get("https://link.springer.com/signup-login");
        //driver.findElement(By.cssSelector("button[data-cc-action='reject']")).click();

        driver.findElement(By.id("login-email")).sendKeys(email);
        driver.findElement(By.id("email-submit")).click();
        driver.findElement(By.id("login-password")).sendKeys(password);
        driver.findElement(By.id("password-submit")).click();


    }
}
