package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Main {
    public static void main(String[] args) {

        WebDriver driver = new FirefoxDriver();
        AdvancedSearch as = new AdvancedSearch(driver);
        as.acceptCookies();

        as.enterAllWords("Page Object Model");
        as.enterLeastWords("Sellenium Testing");
        as.selectIn();
        as.enterStartYear("2024");
        as.clickSubmit();
        System.out.println(as.getSearchResults());






    }
}