package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.lang.model.element.Element;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class TestingWeb {
    private WebDriver driver;

    public TestingWeb(WebDriver driver){
        this.driver = driver;
    }

    public void navigateToLoginPage() {
        driver.get("https://link.springer.com/");
        driver.findElement(By.cssSelector("button[data-cc-action='reject']")).click();
        driver.findElement(By.id("identity-account-widget")).click();
    }
    public void register(String email,String name, String familyName,  String password) {
        driver.findElement(By.cssSelector("button[data-cc-action='reject']")).click();

        driver.findElement(By.id("login-email")).sendKeys(email);
        driver.findElement(By.id("email-submit")).click();
        driver.findElement(By.id("registration-given-names")).sendKeys(name);
        driver.findElement(By.id("registration-family-name")).sendKeys(familyName);
        driver.findElement(By.id("registration-password")).sendKeys(password);
        driver.findElement(By.id("registration-password-confirm")).sendKeys(password);
        ((JavascriptExecutor) driver)
                .executeScript("window.scrollTo(0, document.body.scrollHeight)");
        WebElement checkbox = driver.findElement(By.id("registration-terms-conditions"));
        Actions actions = new Actions(driver);
        actions.moveToElement(checkbox).click().perform();
       driver.findElement(By.id("form-registration-submit")).click();



    }

     public void login(String email, String password){
         driver.get("https://link.springer.com/signup-login");
         //driver.findElement(By.cssSelector("button[data-cc-action='reject']")).click();

         driver.findElement(By.id("login-email")).sendKeys(email);
         driver.findElement(By.id("email-submit")).click();
         driver.findElement(By.id("login-password")).sendKeys(password);
         driver.findElement(By.id("password-submit")).click();


     }
    public void advancedSearch(){
        driver.get("https://link.springer.com/advanced-search");
    }


    public List<Article> getSearchResults() {
        List<Article> articles = new ArrayList<>();
        List<WebElement> resultList = driver.findElements(By.cssSelector("li.app-card-open.app-card-open--has-image[data-test=search-result-item]"));
        int count = 1;
        for (WebElement resultElement : resultList) {
            // Title
            WebElement titleElement = resultElement.findElement(By.cssSelector("h3.app-card-open__heading a"));
            String title = titleElement.getText();
            // DOI
            String href = titleElement.getAttribute("href");
            String doi = href.substring(href.indexOf("/article/") + 9, href.indexOf("/", href.indexOf("/article/") + 9));
            // Publication date
            WebElement metaElement = resultElement.findElement(By.cssSelector("div.c-meta span[data-test=published]"));
            String publishedDate = metaElement.getText();

            WebElement metaElementContentType = resultElement.findElement(By.cssSelector("div.c-meta span[data-test=content-type]"));
            String contentType = metaElementContentType.getText();
            if(contentType.equals("Article")) {
                articles.add(new Article(title, doi, publishedDate, contentType));
                if (count == 4) {
                    break;
                }

                count++;
            }
        }
        return articles;
    }

}
