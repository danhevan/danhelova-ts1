package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class searching {
    private WebDriver driver;

    public searching(WebDriver driver) {
        this.driver = driver;
        driver.get("https://link.springer.com/search");
        searchInput = driver.findElement(By.id("search-springerlink"));
        searchButton = driver.findElement(By.id("search-submit"));
    }
    private WebElement searchInput;

    private WebElement searchButton ;

    public void rejectCookies(){
            WebElement reject = driver.findElement(By.cssSelector("button[data-cc-action='reject']"));
            reject.click();

    }

    public void enterSearchKeyword(String keyword) {
        searchInput.sendKeys(keyword);
    }

    public void submitSearch() {
        searchButton.click();
    }

    public Article result(){
        List<WebElement> resultList = driver.findElements(By.cssSelector("li.app-card-open.app-card-open--has-image[data-test=search-result-item]"));
        for (WebElement resultElement : resultList) {
            // Title
            WebElement titleElement = resultElement.findElement(By.cssSelector("h3.app-card-open__heading a"));
            String title = titleElement.getText();
            // DOI
            String href = titleElement.getAttribute("href");
            String doi = href.substring(href.indexOf("/article/") + 9, href.indexOf("/", href.indexOf("/article/") + 9));
            // Publication date
            WebElement metaElement = resultElement.findElement(By.cssSelector("div.c-meta span[data-test=published]"));
            String publishedDate = metaElement.getText();

            WebElement metaElementContentType = resultElement.findElement(By.cssSelector("div.c-meta span[data-test=content-type]"));
            String contentType = metaElementContentType.getText();
            if(contentType.equals("Article")) {
                Article article = new Article(title, doi, publishedDate, contentType);
                return article;
            }
        }
        return null;
    }

}


