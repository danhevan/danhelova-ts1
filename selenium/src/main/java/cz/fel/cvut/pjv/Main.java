package cz.fel.cvut.pjv;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");

        WebDriver driver = new FirefoxDriver();
        driver.get("https://ts1.V-sources.eu");
        driver.findElement(By.id("flight_form_firstName")).sendKeys("Honza");
        driver.findElement(By.id("flight_form_lastName")).sendKeys("Polívka");
        driver.findElement(By.id("flight_form_email")).sendKeys("Polívka@seznam.cz");
        Select selectDestination = new Select(driver.findElement(By.id("flight_form_destination")));
        selectDestination.selectByVisibleText("London");
        driver.findElement(By.cssSelector("body > div > div > form > div > button")).click();


    }
}