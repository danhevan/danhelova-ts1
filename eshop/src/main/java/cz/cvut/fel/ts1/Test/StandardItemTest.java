package cz.cvut.fel.ts1.Test;


import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeFalse;

class StandardItemTest {
    StandardItem item;
    @BeforeEach
    void makeItem(){
        item = new StandardItem(66, "Name", 666, "Category", 6);
    }

    @Test
    void testConstructor() {
        assertEquals(66, item.getID());
        assertEquals("Name", item.getName());
        assertEquals(666, item.getPrice());
        assertEquals("Category", item.getCategory());
        assertEquals(6, item.getLoyaltyPoints());
        assertNotNull(item);
    }
    @Test
    void testCopy() {
        assertEquals(item.getID(), item.copy().getID());
        assertEquals(item.getName(), item.copy().getName());
        assertEquals(item.getPrice(), item.copy().getPrice());
        assertEquals(item.getCategory(), item.copy().getCategory());
        assertEquals(item.getLoyaltyPoints(), item.copy().getLoyaltyPoints());

    }

    @Test
    void testEquals() {
        assertTrue(item.equals(item.copy()));
        StandardItem item2 = new StandardItem(00, "Name", 666, "Category", 6);
        StandardItem item3 = new StandardItem(66, "DifferentName", 666, "Category", 6);
        StandardItem item4 = new StandardItem(66, "Name", 66, "Category", 6);
        StandardItem item5 = new StandardItem(66, "Name", 666, "DifferentCategory", 6);
        StandardItem item6 = new StandardItem(66, "Name", 666, "Category", 9);
        assumeFalse(item.equals(item2));
        assumeFalse(item.equals(item3));
        assumeFalse( item.equals(item4));
        assumeFalse(item.equals(item5));
        assumeFalse( item.equals(item6));
    }



}