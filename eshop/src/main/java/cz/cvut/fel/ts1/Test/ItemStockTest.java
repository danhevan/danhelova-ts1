package cz.cvut.fel.ts1.Test;

import cz.cvut.fel.ts1.shop.StandardItem;
import cz.cvut.fel.ts1.storage.ItemStock;
import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

class ItemStockTest {
    public ItemStockTest(){
        StandardItem item = new StandardItem(44, "Name", 66, "Cat", 4);
        ItemStock itemStock = new ItemStock(item);
        this.itemStock = itemStock;
        this.item = item;

    }

    ItemStock itemStock;
    StandardItem item;



    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4})
    void parametrizedTest(int input){
        itemStock.IncreaseItemCount(input);
        assertEquals(input, itemStock.getCount());
    }
    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4})
    void parametrizedTest2(int input){
        itemStock.decreaseItemCount(input);
        assertEquals(-input, itemStock.getCount());
    }

    @Test
    void testConstructor(){
        assertEquals(this.item, itemStock.getItem());
    }



}
