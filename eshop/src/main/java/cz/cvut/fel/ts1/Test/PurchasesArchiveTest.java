package cz.cvut.fel.ts1.Test;

import cz.cvut.fel.ts1.archive.ItemPurchaseArchiveEntry;
import cz.cvut.fel.ts1.archive.PurchasesArchive;
import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.Order;
import cz.cvut.fel.ts1.shop.ShoppingCart;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

class PurchasesArchiveTest {
    PurchasesArchive purchasesArchive = new PurchasesArchive();
    StandardItem item = new StandardItem(44, "Name", 66, "Cat", 4);
    StandardItem item2 = new StandardItem(442, "Name2", 666, "Cat2", 42);
    ArrayList<ItemPurchaseArchiveEntry > itemList = new ArrayList<ItemPurchaseArchiveEntry >();
    ArrayList<Item> itemList2 = new ArrayList<Item>();
    String out;
    ShoppingCart shoppingCart;
    Order order;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;


public PurchasesArchiveTest(){
    this.itemList.add(new ItemPurchaseArchiveEntry(this.item));
    this.itemList2.add(this.item);
    this.shoppingCart = new ShoppingCart(this.itemList2);
    this.order = new Order(this.shoppingCart,"CustomerName", "CustomerAddress",6);
    this.purchasesArchive.putOrderToPurchasesArchive(this.order);
    System.setOut(new PrintStream(outContent));
    this.out = "ITEM PURCHASE STATISTICS:\n" +
            "ITEM  Item   ID 44   NAME Name   CATEGORY Cat   PRICE 66.0   LOYALTY POINTS 4   HAS BEEN SOLD 1 TIMES\n";
}





    @Test
    void getHowManyTimesHasBeenItemSold() {
    assertEquals(1,this.purchasesArchive.getHowManyTimesHasBeenItemSold(item));
    }

    @Test
    void putOrderToPurchasesArchive() {
    assertEquals(this.order, this.purchasesArchive.orderArchive.get(0));
    }

    @Test
    void output() {
        this.purchasesArchive.printItemPurchaseStatistics();
        assertEquals(out, outContent.toString()) ;
    }

}

