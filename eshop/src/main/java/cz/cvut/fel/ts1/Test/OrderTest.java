package cz.cvut.fel.ts1.Test;

import cz.cvut.fel.ts1.shop.Order;
import cz.cvut.fel.ts1.shop.ShoppingCart;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {
    Order order1;
    ShoppingCart shoppingCart = new ShoppingCart();

    @BeforeEach
    void setUp() {

    }
    @Test
    void testNullConstructor1(){
    assertThrows(NullPointerException.class,
            ()->{order1 = new Order(null, null, null, 0);
    });
        assertThrows(NullPointerException.class,
                ()->{order1 = new Order(null, null, null);
                });
    }
    @Test
    void testConstructor1(){
        order1 = new Order(shoppingCart, "CustomerName","CustomerAddress", 6);
        assertEquals("CustomerName", order1.getCustomerName());
        assertEquals("CustomerAddress", order1.getCustomerAddress());
        assertEquals(6, order1.getState());

    }
    @Test
    void testConstructor2(){
        order1 = new Order(shoppingCart, "CustomerName","CustomerAddress");
        assertEquals("CustomerName", order1.getCustomerName());
        assertEquals("CustomerAddress", order1.getCustomerAddress());
    }
}