package cz.fel.cvut.pjv;

public class Calculator {
    public int addition (int cislo1, int cislo2){
        return cislo1 + cislo2;
    }
    public int subtraction (int cislo1, int cislo2){
        return cislo1 - cislo2;
    }

    public int multiplication (int cislo1, int cislo2){
        return cislo1 * cislo2;
    }
    public double division (double cislo1, double cislo2){
        return cislo1/cislo2;
    }
}
