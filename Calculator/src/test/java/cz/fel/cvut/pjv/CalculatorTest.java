package cz.fel.cvut.pjv;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class CalculatorTest {
    Calculator calculator = new Calculator();
@Test
    public void testAddition(){
    int output = calculator.addition(1,2);
    assertEquals(3, output);
    }
    @Test
    public void testSubtraction(){
        int output = calculator.subtraction(5,2);
        assertEquals(3, output);
    }
    @Test
    public void testMultiplication(){
        int output = calculator.multiplication(1,3);
        assertEquals(3, output);
    }
    @Test
    public void testDivision(){
        double output = calculator.division(9,3);
        assertEquals(3, output);
    }
    @Test
    public void testDivisionExeption() throws ArithmeticException{
        calculator.division(1,0);
    }

}